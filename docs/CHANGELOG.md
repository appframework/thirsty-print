## [1.0.4](https://gitlab.com/appframework/thirsty-print/compare/1.0.3...1.0.4) (2025-02-26)


### Bug Fixes

* **deps:** update dependency org.slf4j:slf4j-api to v2.0.17 ([56093b1](https://gitlab.com/appframework/thirsty-print/commit/56093b1f64cd10e17fcdc6fd04363331385559f0))

## [1.0.3](https://gitlab.com/appframework/thirsty-print/compare/1.0.2...1.0.3) (2024-12-31)


### Bug Fixes

* **deps:** update dependency ch.qos.logback:logback-classic to v1.3.15 ([2906b23](https://gitlab.com/appframework/thirsty-print/commit/2906b23a3333f7e5a3edf4b346513762863d4c4e))
* **deps:** update dependency com.fasterxml.jackson.core:jackson-databind to v2.18.2 ([41ee10a](https://gitlab.com/appframework/thirsty-print/commit/41ee10ac7da69adf772115397b91f5c82fc80106))

## [1.0.2](https://gitlab.com/appframework/thirsty-print/compare/1.0.1...1.0.2) (2024-10-29)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.core:jackson-databind to v2.18.1 ([797d491](https://gitlab.com/appframework/thirsty-print/commit/797d4916c48f35a555230825a5a4a2ec508d64d8))

## [1.0.1](https://gitlab.com/appframework/thirsty-print/compare/1.0.0...1.0.1) (2024-10-28)

# 1.0.0 (2024-10-21)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.core:jackson-databind to v2.18.0 ([3efe8c1](https://gitlab.com/appframework/thirsty-print/commit/3efe8c1cc42b3d8dff3adfc3a5caed6c95431e89))
