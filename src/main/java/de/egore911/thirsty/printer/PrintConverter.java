package de.egore911.thirsty.printer;

import jpos.JposException;
import jpos.POSPrinterControl114;

class PrintConverter {

    private PrintConverter() {
    }

    public static long getRecLineChars(POSPrinterControl114 ptr, int[] recLineChars, int maxLineWidths) throws JposException {
        long lRecLineChars;
        long lCount;
        int i;

        // Calculate the element count.
        String[] temp = ptr.getRecLineCharsList().split(",");
        lCount = temp[0].length();

        if (lCount == 0) {
            lRecLineChars = 0;
        } else {
            if (lCount > maxLineWidths) {
                lCount = maxLineWidths;
            }

            for (i = 0; i < lCount; i++) {
                recLineChars[i] = Integer.parseInt(temp[i]);
            }

            lRecLineChars = lCount;
        }

        return lRecLineChars;
    }

    /**
     * Outline      An appropriate interval is converted into the length of
     * the tab about two texts.
     * And make a printing data.
     *
     * @param lineChars The width of the territory which it
     *                  prints on is converted into the number of
     *                  characters, and that value is specified.
     * @param text1     It is necessary as an information for
     *                  deciding the interval of the text.
     * @param text2     It is necessary as an information for
     *                  deciding the interval of the text, too.
     * @return printing data.
     */
    public static String makePrintString(int lineChars, String text1, String text2) {
        String tab = "";
        try {
            int spaces = lineChars - (text1.length() + text2.length());
            tab = " ".repeat(Math.max(0, spaces));
        } catch (Exception ignored) {
            // Ok, that didn't work
        }
        return text1 + tab + text2;
    }

}
