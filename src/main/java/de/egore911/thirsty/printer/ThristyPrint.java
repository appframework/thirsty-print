package de.egore911.thirsty.printer;

import jpos.JposException;
import jpos.util.JposPropertiesConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jakarta.websocket.CloseReason;
import jakarta.websocket.CloseReason.CloseCodes;
import jakarta.websocket.DeploymentException;

public class ThristyPrint {

    private static final Logger LOG = LoggerFactory.getLogger(ThristyPrint.class);

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException {

        if (args.length < 1) {
            System.err.println("Please pass URL as first parameter (e.g. https://localhost:8443/thirsty");
            return;
        }

        String url = args[0];
        Matcher matcher = Pattern.compile("^http(s?)://").matcher(url);
        StringBuffer buffer = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(buffer, "ws$1://");
        }
        matcher.appendTail(buffer);
        url = buffer.toString();
        if (!url.endsWith("/")) {
            url += "/";
        }

        LOG.debug("Starting up...");

        Class.forName("jp.co.epson.pos.comm.v4_0001.PortHandlerIO").getConstructor((Class[]) null)
                .newInstance((Object[]) null);

        System.setProperty(JposPropertiesConst.JPOS_POPULATOR_FILE_PROP_NAME, "thirsty-printer.xml");
        LOG.debug("Set JPOS_POPULATOR_FILE_PROP_NAME to {}",
                System.getProperty(JposPropertiesConst.JPOS_POPULATOR_FILE_PROP_NAME));

        if (!Files.exists(Paths.get("thirsty-printer.xml"))) {
            throw new RuntimeException("Missing thirsty-printer.xml in current directory");
        }

        try (PrinterHandle handle = new PrinterHandle("TM-T20II-Ethernet")) {
            try {
                handle.printStatus();
            } catch (JposException | SocketException e) {
                LOG.warn("Could not print status: {}", e.getMessage(), e);
            }

            CloseReason reason = null;
            do {
                try (PrinterClientEndpoint endpoint = new PrinterClientEndpoint(new URI(url + "async/printer"),
                        handle)) {
                    while (endpoint.isOpen()) {
                        Thread.sleep(5000);
                    }
                    reason = endpoint.getCloseReason();
                } catch (DeploymentException | IOException e) {
                    LOG.warn("Retrying, but got message: {}", e.getMessage());
                }
            } while (reason == null || (reason.getCloseCode() == CloseCodes.GOING_AWAY) || (reason.getCloseCode() == CloseCodes.NORMAL_CLOSURE));
            LOG.info("Close reason: {}", reason);
        } catch (InterruptedException | URISyntaxException | JposException e) {
            LOG.error(e.getMessage(), e);
        }
    }
}
