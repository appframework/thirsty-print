package de.egore911.thirsty.printer;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.egore911.thirsty.printer.model.OrderStatusUpdate;
import de.egore911.thirsty.printer.model.PrintJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.websocket.ClientEndpoint;
import jakarta.websocket.CloseReason;
import jakarta.websocket.ContainerProvider;
import jakarta.websocket.DeploymentException;
import jakarta.websocket.OnClose;
import jakarta.websocket.OnMessage;
import jakarta.websocket.OnOpen;
import jakarta.websocket.Session;
import jakarta.websocket.WebSocketContainer;
import java.io.Closeable;
import java.io.IOException;
import java.net.URI;

@ClientEndpoint
public class PrinterClientEndpoint implements Closeable {

    private static final Logger LOG = LoggerFactory.getLogger(PrinterClientEndpoint.class);

    private Session session;
    private final PrinterHandle handle;
    private final ObjectMapper mapper = new ObjectMapper();

    private CloseReason reason;

    PrinterClientEndpoint(URI endpointURI, PrinterHandle handle) throws DeploymentException, IOException {
        this.handle = handle;
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        container.connectToServer(this, endpointURI);
        LOG.info("Connected to server {}", endpointURI);
    }

    @OnOpen
    public void onOpen(Session session) {
        LOG.debug("Opening session {}", session.getId());
        this.session = session;
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        LOG.debug("Closing session {}: {}", session.getId(), reason);
        this.session = null;
        this.reason = reason;
    }

    boolean isOpen() {
        return session != null;
    }

    @OnMessage
    public void onMessage(String message) {
        try {
            PrintJob job = mapper.readValue(message, PrintJob.class);
            if (job == null) {
                LOG.debug("Received no job, waiting");
                return;
            }
            LOG.info("Received job #{}, printing", job.getOrderNumber());
            handle.print(job);
            session.getAsyncRemote().sendText(mapper.writeValueAsString(new OrderStatusUpdate(job.getOrderNumber(), "PRINTED")));
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public void close() {
        if (session != null) {
            try {
                session.close();
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
            }
        }
    }
    
    public CloseReason getCloseReason() {
        return reason;
    }
}