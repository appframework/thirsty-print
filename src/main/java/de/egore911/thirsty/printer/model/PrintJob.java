package de.egore911.thirsty.printer.model;

import java.util.ArrayList;
import java.util.List;

public class PrintJob {

    private String orderedBy;
    private int orderNumber;
    private List<OrderEntry> entries = new ArrayList<>(0);
    private String comment;
    private List<String> freetextEntries = new ArrayList<>(0);

    public String getOrderedBy() {
        return orderedBy;
    }

    public void setOrderedBy(String orderedBy) {
        this.orderedBy = orderedBy;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<OrderEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<OrderEntry> entries) {
        this.entries = entries;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<String> getFreetextEntries() {
        return freetextEntries;
    }

    public void setFreetextEntries(List<String> freetextEntries) {
        this.freetextEntries = freetextEntries;
    }

}
