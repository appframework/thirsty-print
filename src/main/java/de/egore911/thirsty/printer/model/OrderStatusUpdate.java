package de.egore911.thirsty.printer.model;

public class OrderStatusUpdate {

    private int orderId;
    private String status;

    public OrderStatusUpdate() {
    }

    public OrderStatusUpdate(int orderId, String status) {
        this.orderId = orderId;
        this.status = status;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
