package de.egore911.thirsty.printer;

import de.egore911.thirsty.printer.model.OrderEntry;
import de.egore911.thirsty.printer.model.PrintJob;
import jp.co.epson.upos.UPOSConst;
import jpos.JposException;
import jpos.POSPrinter;
import jpos.POSPrinterConst;
import jpos.POSPrinterControl114;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.Closeable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Time;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.StringTokenizer;

import static de.egore911.thirsty.printer.PrintConverter.getRecLineChars;
import static de.egore911.thirsty.printer.PrintConverter.makePrintString;

public class PrinterHandle implements Closeable {

    private static final Logger LOG = LoggerFactory.getLogger(PrinterHandle.class);

    private final POSPrinterControl114 ptr;

    private static final int MAX_LINE_WIDTHS = 2;

    public PrinterHandle(String name) throws JposException {
        LOG.debug("Starting printer");
        ptr = new POSPrinter();
        ptr.open(name);
        LOG.info("Printer {} opened, verion: {}", name, ptr.getDeviceServiceVersion());
        ptr.claim(1000);
        LOG.info("Printer {} claimed", name);
        ptr.setDeviceEnabled(true);
        LOG.info("Printer {} enabled", name);
        ptr.setMapMode(POSPrinterConst.PTR_MM_METRIC);
        ptr.setRecLetterQuality(true);
        LOG.info("Printer {} dimensions set", name);

        uploadImage();
        LOG.info("Printer {} image uploaded", name);
    }

    private void uploadImage() throws JposException {
        if (Files.exists(Path.of("logo.bmp")) && ptr.getCapRecBitmap()) {
            for (int iRetryCount = 0; iRetryCount < 5; iRetryCount++) {
                try {
                    //Register a bitmap
                    ptr.setBitmap(1, POSPrinterConst.PTR_S_RECEIPT, "logo.bmp",
                            (ptr.getRecLineWidth() / 2), POSPrinterConst.PTR_BM_CENTER);
                    break;
                } catch (JposException ex) {
                    if (ex.getErrorCode() == UPOSConst.UPOS_E_FAILURE && ex.getErrorCodeExtended() == 0 && ex.getMessage().equals("It is not initialized.")) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex2) {
                        }
                    }
                }
            }
        }
    }

    void printStatus() throws JposException, SocketException {
        if (ptr.getCapRecPresent()) {

            // Start batch
            ptr.transactionPrint(POSPrinterConst.PTR_S_RECEIPT, POSPrinterConst.PTR_TP_TRANSACTION);

            // Print bitmap
            if (ptr.getCapRecBitmap()) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|1B");
            }

            // 5mm space
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|500uF");

            Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
            while (n.hasMoreElements()) {
                NetworkInterface e = n.nextElement();

                Enumeration<InetAddress> a = e.getInetAddresses();
                while (a.hasMoreElements()) {
                    InetAddress addr = a.nextElement();
                    if (!addr.isLoopbackAddress() && addr instanceof Inet4Address) {
                        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|Nhttp://" + addr.getHostAddress() + "/thirsty\n");
                    }
                }
            }
            // 5mm space
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|500uF");

            // Line Feed and Paper cut
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|fP");

            // Complete batch
            ptr.transactionPrint(POSPrinterConst.PTR_S_RECEIPT, POSPrinterConst.PTR_TP_NORMAL);
        }

    }

    void print(@Nonnull PrintJob job) {
        DateFormat df = DateFormat.getDateInstance();
        Time t = new Time(System.currentTimeMillis());

        String time = df.format(Calendar.getInstance().getTime()) + ' ' + t + '\n';

        int[] recLineChars = new int[MAX_LINE_WIDTHS];
        long recLineCharsCount;

        try {
            if (ptr.getCapRecPresent()) {

                // Start batch
                ptr.transactionPrint(POSPrinterConst.PTR_S_RECEIPT, POSPrinterConst.PTR_TP_TRANSACTION);

                // Print bitmap
                if (ptr.getCapRecBitmap()) {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|1B");
                }

                // Print ordering person
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|N" + job.getOrderedBy() + '\n');
                // Print order number
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA#" + job.getOrderNumber() + '\n');

                // 2mm space
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|200uF");

                //Print date
                //   ESC|cA = Centering char
                recLineCharsCount = getRecLineChars(ptr, recLineChars, MAX_LINE_WIDTHS);
                String charsList = ptr.getRecLineCharsList();
                String charListArray[] = new String[MAX_LINE_WIDTHS];
                int iCounter = 0;
                StringTokenizer st = new StringTokenizer(charsList, ",");
                while (st.hasMoreTokens()) {
                    charListArray[iCounter] = st.nextToken();
                    iCounter++;
                }
                for (int i = 0; i < iCounter; i++) {
                    if (charListArray[i] != null) {
                        recLineChars[i] = Integer.parseInt(charListArray[i]);
                    }
                }
                if (recLineCharsCount >= 2) {
                    ptr.setRecLineChars(recLineChars[1]);
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + time + '\n');
                    ptr.setRecLineChars(recLineChars[0]);
                } else {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + time + '\n');
                }

                // Print normal orders
                int total = 0;
                String printData;
                for (int i = 0; i < job.getEntries().size(); i++) {
                    OrderEntry entry = job.getEntries().get(i);
                    printData = makePrintString(ptr.getRecLineChars(), entry.getName(), Integer.toString(entry.getAmount()));
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, (i == job.getEntries().size() - 1 ? "\u001b|uC" : "") + printData + '\n');
                    total += entry.getAmount();
                }

                // 2mm space
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|200uF");

                // Print summary
                printData = makePrintString((ptr.getRecLineChars() / 2), "Anzahl Getränke", Integer.toString(total));
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|bC\u001b|2C" + printData + '\n');

                if (!job.getEntries().isEmpty() && !job.getFreetextEntries().isEmpty()) {
                    // 2mm space
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|200uF");
                }

                // Print freetext orders
                for (int i = 0; i < job.getFreetextEntries().size(); i++) {
                    String content = job.getFreetextEntries().get(i);
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|N" + content + '\n');
                }

                // Print comment
                if (job.getComment() != null && !job.getComment().isEmpty()) {
                    // 2mm space
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|200uF");

                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|N" + job.getComment() + '\n');
                }

                // 5mm space
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|500uF");

                // Line Feed and Paper cut
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|fP");

                // Complete batch
                ptr.transactionPrint(POSPrinterConst.PTR_S_RECEIPT, POSPrinterConst.PTR_TP_NORMAL);
            }
        } catch (JposException e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        try {
            //Cancel the device.
            ptr.setDeviceEnabled(false);

            //Release the device exclusive control right.
            ptr.release();

            //Finish using the device.
            ptr.close();
        } catch (JposException ex) {
            throw new RuntimeException(ex);
        }
    }
}
