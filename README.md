# Setup

## System setup

1. Install "EPSON JavaPOS ADK (for install automatically) Ver. 1.14.6W"
2. Assure the following DLLs are in your path (e.g. in your JRE/bin directory):
   - BluetoothIO.DLL
   - epsonjpos.dll
   - EthernetIO31.DLL
   - SerialIO31.dll
   - USBIO31.DLL

## Epson TM-T20II ethernet setup

1. Install EpsonNetConfig_v4_9_4.exe
2. Run it
3. Click on "TM-T20II" which has "-- NONE --" as IP adress (just MAC). Wait for a really long time.
4. Set Mode to automatic
5. Wait for really long time

## (Optional) development setup for maven

    mvn install:install-file -Dfile=jpos1141.jar -DgroupId=org.jpos \
        -DartifactId=jpos1141 -Dversion=0 -Dpackaging=jar

    mvn install:install-file -Dfile=epsonjpos.jar -DgroupId=com.epson \
        -DartifactId=epsonjpos -Dversion=0 -Dpackaging=jar

# Configuration

1. Copy thirsty-printer.xml.example to the working directory and name it Copy thirsty-printer.xml
2. Start EpsonNet Config
3. Press "Refresh" until printer appears
4. Set IP in thirsty-printer.xml in property "PortName"
5. Optionally: Create logo.bmp in working directory (244x134, 1 bit color)

# Usage

Main class: Thirsty Print
Arguments:
1. URL of thirsty-web installation

